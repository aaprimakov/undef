import React from 'react';
import Lottie from 'react-lottie';
import * as animationData from './404.json'

const App = () => {
    return(
        <div>
            <h1 style={{ textAlign: 'center', color: '#3f7afe' }}>Undefined app name</h1>
            <p style={{ textAlign: 'center' }}>
                Вы запросили приложение по адресу undefined. Возможно не указана версия в админке или не сброшен кэш
            </p>
            <p style={{ textAlign: 'center' }}>
                You requested an application by url -&gt; undefined. Perhaps the version is not specified in the admin panel or the cache has not been reset.
            </p>
            <Lottie
                options={{
                    loop: true,
                    autoplay: true, 
                    animationData: animationData,
                    rendererSettings: {
                      preserveAspectRatio: 'xMidYMid slice'
                    }
                  }}
                style={{ maxWidth: 600 }}
            />
        </div>
    )
}

export default App;

